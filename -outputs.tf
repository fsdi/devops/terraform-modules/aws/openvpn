# ssh-key

output "ssh_key_name" {
  description = "Name of the keypair"
  value       = aws_key_pair.keypair.key_name
}

output "ssh_key_secret_manager_name" {
  description = "Name of the keypair"
  value       = aws_secretsmanager_secret.secret_key.name
}

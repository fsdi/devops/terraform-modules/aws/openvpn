PROJECT_ID 	= 
ENV 		= 


tfi:
	@terraform init

tfp:
	@terraform plan -out tfplan

tfa:
	@terraform apply tfplan

tfpa: tfi tfp tfa

tfs: tfi
	terraform  show | grep '^#'

tfd: tfi
	terraform  plan -destroy  -out tfdestroy && \
	echo "Destroying !!! $(PWD) via terraform" && \
	sleep 45 && \
	terraform apply tfdestroy
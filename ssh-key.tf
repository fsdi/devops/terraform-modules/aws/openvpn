resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "keypair" {
  key_name   = var.name
  public_key = tls_private_key.key.public_key_openssh
}

resource "local_file" "ssh_key" {
  filename = "${path.module}/id_rsa.pub"
  content  = aws_key_pair.keypair.public_key
}

resource "local_sensitive_file" "ssh_key" {
  filename        = "${path.module}/id_rsa"
  content         = tls_private_key.key.private_key_pem
  file_permission = "0600"
}

resource "aws_secretsmanager_secret" "secret_key" {
  name        = "${var.name}-${random_string.secret_uid.result}"
  description = "${var.name}-key is a secret Key for ${var.name}"
  tags = {
    Name = "${var.name}-key"
  }

}

resource "aws_secretsmanager_secret_version" "secret_key_value" {
  secret_id     = aws_secretsmanager_secret.secret_key.id
  secret_string = tls_private_key.key.private_key_pem
}


# VPN relates variables

variable "openvpn_ssh_user" {
  default = "openvpnas"
}

variable "openvpn_ssh_port" {
  default = 22
}

variable "openvpn_ssh_cidr" {
  default = "0.0.0.0/0"
}

variable "openvpn_https_port" {
  default = 443
}

variable "openvpn_https_cidr" {
  default = "0.0.0.0/0"
}

variable "openvpn_tcp_port" {
  default = 943
}

variable "openvpn_tcp_cidr" {
  default = "0.0.0.0/0"
}

variable "openvpn_udp_port" {
  default = 1194
}

variable "openvpn_udp_cidr" {
  default = "0.0.0.0/0"
}

variable "certificate_email" {
}

variable "name" {
}
variable "domain_name" {
}

variable "zone_id" {
}

variable "ami_id" {
}


variable "instance_type" {
  default = "t2.small"
}

variable "vpc_id" {
}

variable "vpc_cidr" {
}

variable "subnet_id" {
}

variable "openvpn_admin_user" {
  default = "openvpn"
}

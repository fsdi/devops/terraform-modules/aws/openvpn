# openvpn password

resource "random_password" "password" {
  length           = 16
  special          = true
  override_special = "_%@"
}

resource "random_string" "secret_uid" {
  length           = 4
  special          = false
}

resource "aws_secretsmanager_secret" "openvpn_password" {
  name        = "${var.name}-credentials-${random_string.secret_uid.result}"
  description = "${var.name}-key is a secret Key for ${var.name}"
  tags = {
    Name = "${var.name}-key"
  }

}

locals {
  credentials = {
    username = var.openvpn_ssh_user
    password = random_password.password.result
  }

}

resource "aws_secretsmanager_secret_version" "openvpn_credentials" {
  secret_id     = aws_secretsmanager_secret.openvpn_password.id
  secret_string = jsonencode(local.credentials)
}

# sg

resource "aws_security_group" "openvpn" {
  name        = "openvpn_sg"
  description = "Allow traffic needed by openvpn"
  vpc_id      = var.vpc_id

}

resource "aws_security_group_rule" "openvpn_egr_all" {
  security_group_id = aws_security_group.openvpn.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "openvpn_ssh" {
  security_group_id = aws_security_group.openvpn.id
  description       = "ssh for mgmt"
  type              = "ingress"
  from_port         = var.openvpn_ssh_port
  to_port           = var.openvpn_ssh_port
  protocol          = "tcp"
  cidr_blocks       = [var.openvpn_ssh_cidr]
}

resource "aws_security_group_rule" "openvpn_https" {
  security_group_id = aws_security_group.openvpn.id
  description       = "https web-ui"
  type              = "ingress"
  from_port         = var.openvpn_https_port
  to_port           = var.openvpn_https_port
  protocol          = "tcp"
  cidr_blocks       = [var.openvpn_https_cidr]
}

resource "aws_security_group_rule" "openvpn_http" {
  security_group_id = aws_security_group.openvpn.id
  description       = "http for lets encrypt certbot"
  type              = "ingress"
  from_port         = "80"
  to_port           = "80"
  protocol          = "tcp"
  cidr_blocks       = [var.openvpn_https_cidr]
}

resource "aws_security_group_rule" "openvpn_vpn_udp" {
  security_group_id = aws_security_group.openvpn.id
  description       = "openvpn udp port"
  type              = "ingress"
  from_port         = var.openvpn_udp_port
  to_port           = var.openvpn_udp_port
  protocol          = "udp"
  cidr_blocks       = [var.openvpn_udp_cidr]
}

resource "aws_security_group_rule" "openvpn_vpn_tcp" {
  security_group_id = aws_security_group.openvpn.id
  description       = "openvpn tcp port"
  type              = "ingress"
  from_port         = var.openvpn_tcp_port
  to_port           = var.openvpn_tcp_port
  protocol          = "tcp"
  cidr_blocks       = [var.openvpn_tcp_cidr]
}

# eip

resource "aws_eip" "openvpn_eip" {
  vpc = true
}

resource "aws_eip_association" "openvpn_eip" {
  instance_id   = aws_instance.openvpn.id
  allocation_id = aws_eip.openvpn_eip.id
}
resource "aws_instance" "openvpn" {
  tags = {
    Name = "openvpn"
  }

  ami                         = var.ami_id
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.keypair.key_name
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = [aws_security_group.openvpn.id]
  associate_public_ip_address = true

  # `admin_user` and `admin_pw` need to be passed in to the appliance through `user_data`, see docs -->
  # https://docs.openvpn.net/how-to-tutorialsguides/virtual-platforms/amazon-ec2-appliance-ami-quick-start-guide/
  user_data = <<USERDATA
admin_user=${var.openvpn_admin_user}
admin_pw=${random_password.password.result}
USERDATA
}

resource "aws_route53_record" "openvpn_public_dns" {
  zone_id = var.zone_id
  name    = "${var.name}.${var.domain_name}"
  type    = "A"
  ttl     = "300"
  records = [aws_eip.openvpn_eip.public_ip]
}

resource "null_resource" "provision_openvpn" {
  depends_on = [aws_instance.openvpn]

  connection {
    type = "ssh"

    host        = aws_eip.openvpn_eip.public_ip
    user        = var.openvpn_ssh_user
    port        = var.openvpn_ssh_port
    private_key = tls_private_key.key.private_key_pem
    agent       = false
  }

  provisioner "remote-exec" {
    inline = [
      "echo Installing Dependencies",
      "sudo apt-get install -y curl vim libltdl7 python3 python3-pip python software-properties-common unattended-upgrades",
      "sudo add-apt-repository -y ppa:certbot/certbot",
      "sudo apt-get -y update",
      "sudo apt-get -y install certbot",
      "sudo certbot certonly --standalone --non-interactive --agree-tos --email ${var.certificate_email} --domains  ${var.name}.${var.domain_name} --pre-hook 'service openvpnas stop' --post-hook 'service openvpnas start'",
      "sudo ln -s -f /etc/letsencrypt/live/${var.name}.${var.domain_name}/cert.pem /usr/local/openvpn_as/etc/web-ssl/server.crt",
      "sudo ln -s -f /etc/letsencrypt/live/${var.name}.${var.domain_name}/privkey.pem /usr/local/openvpn_as/etc/web-ssl/server.key",
      "sudo service openvpnas start",
      "cd /usr/local/openvpn_as/scripts && sudo ./sacli --key 'host.name' --value '${var.name}.${var.domain_name}' ConfigPut",
      "sudo service openvpnas restart",
      # "cd /usr/local/openvpn_as/scripts && sudo ./sacli --key 'vpn.client.routing.reroute_dns' --value 'custom' ConfigPut",
      # "cd /usr/local/openvpn_as/scripts && sudo ./sacli --key 'vpn.server.dhcp_option.dns.0' --value '${var.vpc_cidr}' ConfigPut",
      # "cd /usr/local/openvpn_as/scripts && sudo ./sacli --key 'vpn.server.dhcp_option.domain' --value '${var.domain_name}' ConfigPut",
      # "cd /usr/local/openvpn_as/scripts && sudo ./sacli --key 'vpn.server.routing.gateway_access' --value 'true' ConfigPut",
      "sudo service openvpnas stop",
      "sudo service openvpnas start",
    ]
  }
}

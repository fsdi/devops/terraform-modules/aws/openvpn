# Terraform OpenVpn - AWS

A sample terraform setup for OpenVPN using Let's Encrypt and Certbot to generate certificates

This repository contains the code used in the tutorial: [Using Let’s Encrypt and Certbot to automate the creation of certificates for OpenVPN](http://loige.co/using-lets-encrypt-and-certbot-to-automate-the-creation-of-certificates-for-openvpn/).

Read the article for details and instructions on how to use it.

## 📥 Terraform Inputs

1. `openvpn_ssh_user` - to deploy the openvpn server (default `openvpnas`)
1. `openvpn_admin_user (default `openvpn`)
1. `instance_type` (default `t2.small`)

1. `certificate_email` - to use for let's encrypt certbot - required !
1. `name`         - shortname e.g `openvpn`
1. `domain_name`  - your domain name e.g. `example.com`
1. `zone_id` - route53 zone id to register the record in
1. `ami_id` - see [here](https://aws.amazon.com/marketplace/pp/prodview-y3m73u6jd5srk?sr=0-1&ref_=beagle&applicationId=AWSMPContessa)
1. `vpc_id`
1. `vpc_cidr`
1. `subnet_id`

### Create terraform.auto.tfvars / use as part of your module

```sh
cat<<EOF>>terraform.auto.tfvars
name              = "frankfurt-openvpn" # dns name lowercase !
vpc_id            = "vpc-xxxxxxxxx"
vpc_cidr          = "10.0.0.0/16"
subnet_id         = "subnet-xxxxxxxxx"
domain_name       = "example.com"
zone_id           = "xxxxxxxxx"
instance_type     = "t3.small"
ami_id            = "ami-0764964fdfe99bc31"
certificate_email = "admin@example.com"
EOF

```

## 📓 Terrform plan

1. Create a random password
1. Store it in aws-secrets manager
1. Create ssh-key store in aws-secrets manager
1. Create Security group for openvpn server wiht reuired ports
1. Create ec2 instance from the provided ami
    1. use local-exec to ssh and configure ssl certificate 
    1. Configure openvpn opetions via `sacli` see `main.tf` for details
1. Register node in Route53 zone provided

## 🚀 In Action 

[![asciicast](https://asciinema.org/a/UIQfxVi2m1RfLOtBtFobEF9qx.svg)](https://asciinema.org/a/UIQfxVi2m1RfLOtBtFobEF9qx)